# CSV listing of the benchmark data
`.csv` files contain a header line with `sys_no`, `sys_name` and then specific QC method name, last column named - `RCCSD(T)/CBS` contains reference value.

files:
 - [o24_0.9.csv](./o24_0.9.csv) - data for R=0.9 Req
 - [o24_1.0.csv](./o24_1.0.csv) - data for R=1.0 Req
 - [o24_1.2.csv](./o24_1.2.csv) - data for R=1.2 Req
 - [o24_1.5.csv](./o24_1.5.csv) - data for R=1.5 Req
 - [o24_2.0.csv](./o24_2.0.csv) - data for R=2.0 Req

# python snippet
`example.py` contains example `Python` snippet that loads the files and lists the benchmark values.
