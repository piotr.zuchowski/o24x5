#!/usr/bin/env python3
import sys

try:
    import pandas as pd
except ImportError:
    print("pandas not found! install via 'python3 -m pip install pandas'")
    sys.exit()

DISTANCES = ["0.9", "1.0", "1.2", "1.5", "2.0"]
# Available columns:
# ['sys_no' 'sys_name' 'RHF' 'LC-wPBE' 'LC-wPBE+XDM' 'MP2' 'UMP2' 'SCS-MP2'
# 'SCS-UMP2' 'RCCSD' 'RCCSD-F12a' 'RCCSD-F12b' 'RCCSD(T**)-F12a'
# 'RCCSD(T**)-F12b' 'UCCSD(T**)-F12a' 'UCCSD(T**)-F12b' 'RCCSD(T)/CBS']
# 'RCCSD(T)/CBS' is considered the reference value

# iterate over all distance sets
for distance in DISTANCES:
    # choose the right file
    O24_FILE = f"o24_{distance}.csv"

    print(f"Distance: {distance} R_{{eq}}")
    # load files using pandas
    data = pd.read_csv(O24_FILE, sep=";")

    # list benchmark values for given R=distance
    print(data[["sys_no", "sys_name", "RCCSD(T)/CBS"]])

