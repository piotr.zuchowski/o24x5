This repository contains data files of O24x5 benchmark dataset. The data set is dedicated to testing ab initio quantum chemistry methods for open-shell dimers bound by noncovalent interactions. 

Content

 - `data_O24x5.ods`  : file with all results (LibreOffice format)  

 - `geometries_nomidbond`: directory with xyz files of all geometries for calculations in basis set without midbond functions

 - `geometries_midbond`: directory with xyz files of all geometries for calculations in basis set with midbond functions 

 - [data](./data/README.md): directory with `.csv` files of all presented results

The second line of the xyz format,  which is normally reserved for comments, has encoded information about the system:

na=nmber_of_electrons_of_A  nb=number_of_electrons_of_B sa=number_of_unpaired_electrons_of_A sb=number_of_unpaired_electrons_of_A A=number_of_last_atom_of__monomer 



